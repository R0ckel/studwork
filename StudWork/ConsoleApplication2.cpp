#include <iostream> 
#include <iomanip> 
#include "windows.h"
#include "string.h"
using namespace std;

struct book {
    char cipher[20];
    char authors[40];
    char title[15];
    int year;
    float price;
};

void show_book(book i)
{
    cout << "Назва: " << i.title << endl;
    cout << "Рік видання: " << i.year << endl;
}


int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    char inputauthor[40];
    const int x = 7;
    book A[x] = {   { "abc - 0001" , "author A", "Title First" , 2005 , 90.50 },
                    { "abc - 0002", "author A", "Title Second" , 1998, 193.50 },
                    { "abc - 0003", "author B", "Title Third" , 2001, 84.00 },
                    { "abc - 0004", "author C", "Title Fourth" , 2019, 111.25 },
                    { "abc - 0005", "author B", "Title Fifth" , 2013, 364.50 },
                    { "abc - 0006", "author D", "Title Sixth" , 1974, 60.50 },
                    { "abc - 0007" , "author A" , "Title seventh" , 2000 , 191.00 } };
    cout << "Введіть бажаного для пошуку автора: ";
    cin.getline(inputauthor, 30);
    float b = 0;
    int nom;
    for (int n = 0; n <= (x-1); n++) {
        if (strcmp(A[n].authors, inputauthor) == 0) {
            show_book(A[n]);
            if (A[n].price > b) {
                b = A[n].price;
                nom = n;
            }
            cout << endl;
        }
    }
    (b != 0) ? cout << "Найдорожча книга цього автора - " << A[nom].title <<
        ", коштує: " << b : cout << "Такого автора не знайдено...";
    cout << endl;

    system ("pause");
    return 0;
}
